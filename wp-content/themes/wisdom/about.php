<?php
	/* Template name: About */
	get_header('shop');
	
	the_post();
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
		<div class="aboutHero" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/about-hero.jpg');">

			<div class="aboutHeroContent">
				<div class="aboutHeroContentWrapper">
					<h1>About Wisdom Stone</h1>
					<p>Wisdom Stone is a brand new line of cabinet hardware, finally providing the industry with affordable, yet fashionable options. The unique designs are created by our own design team and are manufactured with the utmost care in our factories. The base of all Wisdom Stone products are made of zinc and are coated in premium metallic finishes or powder coated. For the designs adorned with crystals, we use imported high quality Grade A crystals. These high quality materials make Wisdom Stone products stand out from the overwhelming amount of options available for cabinet hardware. Wisdom Stone was founded by Vanessa Troyer and Chris Farentinos, co-owners of the much larger and more well-known, <strong><u>Architectural Mailboxes</u></strong>. All office operations for Wisdom Stone are performed out of a beachfront office in Redondo Beach, CA.</p>
				</div>
				<ul>
					<li>
 						<a href="">
 							<button>Browse Our Styles</button>
 						</a>
 					</li>
 					<li class="contactBtn">
 						<a href="">
 							<button>Contact Us</button>
 						</a>
 					</li>

 				</ul>
			</div>
		</div>

		<div class="lifestyleSection" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/more-knobs.jpg');">
			<div class="exploreInstagram">
				<div class="exploreInstagramWrapper">
					<div class="exploreInstagramHeader">
						<h5>Our Lifestyle</h5>
						<h2>Explore the Instagram</h2>
					</div>
					<div class="gridThree">
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/pics/shop-ig1.jpg">
							<div class="instagramProductTitle">
								<p>Pretty in Pink! Find the hardware here http://thd.co/2ejjoVQ</p>
							</div>
							<a href="">
	 							<button>Shop</button>
	 						</a>
						</div>
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/pics/shop-ig2.jpg">
							<div class="instagramProductTitle">
								<p>Dream Kitchen</p>
							</div>
							<a href="">
	 							<button>Shop</button>
	 						</a>
						</div>
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/pics/shop-ig3.jpg">
							<div class="instagramProductTitle">
								<p>Update your mid-century home with a modern twist. Check ou...</p>
							</div>
							<a href="">
	 							<button>Shop</button>
	 						</a>
						</div>
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/pics/shop-ig1.jpg">
							<div class="instagramProductTitle">
								<p>Pretty in Pink! Find the hardware here http://thd.co/2ejjoVQ</p>
							</div>
							<a href="">
	 							<button>Shop</button>
	 						</a>
						</div>
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/pics/shop-ig2.jpg">
							<div class="instagramProductTitle">
								<p>Dream Kitchen</p>
							</div>
							<a href="">
	 							<button>Shop</button>
	 						</a>
						</div>
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/pics/shop-ig3.jpg">
							<div class="instagramProductTitle">
								<p>Update your mid-century home with a modern twist. Check ou...</p>
							</div>
							<a href="">
	 							<button>Shop</button>
	 						</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
	
	get_footer();
?>