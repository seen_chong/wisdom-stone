<?php
	get_header();
	
	the_post();
	
	$product_short = get_post_meta(get_the_ID(), 'product_short', true);
	$width  = get_post_meta(get_the_ID(), 'width', true);
	$height = get_post_meta(get_the_ID(), 'height', true);
	$length = get_post_meta(get_the_ID(), 'length', true);
	$product_vars = get_post_meta(get_the_ID(), 'product_vars', true);
	$product_vars = $product_vars['name'];
	
	$first_gallery = $product_vars[0]['colors'][0]['images'];
	$first_crystal = $product_vars[0]['colors'][0]['title'];
	
	$feat_image    = $first_gallery[0];
	$feat_image    = wp_get_attachment_image_src($feat_image, 'full');
	
	$category      = get_the_terms(get_the_ID(), 'product_category');
	$category	  = $category[0]->term_id;
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section">
		<div class="container wow fadeIn" data-wow-delay="0.4s">
			<article id="product" class="clearfix">
				<div class="images">
					<a class="fancybox" href="<?php echo $feat_image[0]; ?>">
						<img class="large img-responsive aligncenter" src="<?php echo $feat_image[0]; ?>" width="599" height="367" alt=" ">
					</a>
					<div class="owl-carousel">
					<?php
						foreach ($first_gallery as $image) {
							$image_src = wp_get_attachment_image_src($image, 'full');
					?>
							<a class="thumb" href="<?php echo $image_src[0]; ?>">
								<img class="img-responsive" src="<?php echo $image_src[0]; ?>" width="133" height="134" alt=" ">
							</a>
					<?php		
						}
					?>
					</div>
				</div>
				<div class="details">
					<h1><?php the_title(); ?></h1>
					<?php echo wpautop($product_short); ?>
					<div class="options">
						<div class="col">
							<div class="label">Color:</div>
						</div>
						<div class="col">
							<div class="select-box">
								<span class="active">Select Your Finish</span>
								<select style="opacity: 0;" id="polish-values">
									<option value="">Select Your Finish</option>
<?php
								$count_vars = 0;
								
								foreach ($product_vars as $product_var) {
?>
									<option value="<?php echo $count_vars; ?>"><?php echo $product_var['title']; ?></option>
<?php									
									$count_vars++;
								}
?>
								</select>
							</div>
						</div>
<?php
						if ($first_crystal == '') {
							$crystals_block_css  = ' style="display: none"';	
							$crystals_block_css1 = ' display: none';	
						}
?>
						<div class="col"<?php echo $crystals_block_css ?>>
							<div class="select-box"<?php echo $crystals_block_css ?>>
								<span class="active">Available Crystal Colors</span>
								<select style="opacity: 0;<?php echo $crystals_block_css1; ?>" id="color-values"></select>
							</div>
						</div>
						<textarea style="display: none" id="variations"><?php echo json_encode($product_vars); ?></textarea>
						<input type="hidden" id="product_id" value="<?php echo get_the_ID(); ?>" />
					</div>
					<div class="dimensions">
						<div class="col">
							<span class="label">Width:</span> <?php echo $width; ?> </div>
						<div class="col">
							<span class="label">Height:</span> <?php echo $height; ?> </div>
						<div class="col">
							<span class="label">Lenght:</span> <?php echo $length; ?> </div>
					</div>
					<div class="info">
						<h4>Product Info:</h4>
						<?php the_content(); ?>
					</div>
				</div>
			</article>
		</div>
	</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ --> 
	
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<aside class="section">
<?php
		$args = array(
			'post_type' 	  => 'product',
			'posts_per_page' => 4,
			'tax_query'	  => array(
				array(
					'taxonomy' => 'product_category',
					'terms'	=> $category
				)
			)
		);
				
		$products = new WP_Query($args);
		
		if ($products->have_posts()) {
?>
			<div class="container wow fadeIn" data-wow-delay="0.4s">
				<h2 class="main-title">More From The Same Family</h2>
				<div class="products-entries clearfix">
<?php
				while ($products->have_posts()) {
					$products->the_post();
						
					$feat_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
?>
					<!-- --> 
					<a class="prod-entry wow fadeIn" data-wow-delay="0.4s" href="<?php echo get_permalink(get_the_ID()) ;?>">
						<img class="img-responsive aligncenter" src="<?php echo $feat_image; ?>" width="296" height="296" alt=" ">
						<h3><?php the_title(); ?></h3>
					</a> 
					<!-- --> 
<?php
				}
?>
				</div>
			</div>
<?php
		}
?>
	</aside>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ --> 
</main>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *end MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ --> 
<?php
	
	get_footer();
?>