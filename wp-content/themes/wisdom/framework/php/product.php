<?php
	add_action('admin_menu', 'product_add_custom_box');
	add_action('save_post', 'product_save_postdata');

	function product_add_custom_box() {
		$server_url = $GLOBALS['HTTP_SERVER_VARS']['REQUEST_URI'];
			
		if (function_exists('add_meta_box')) {
			add_meta_box('product_extra_options1','Short Description', 'product_extra_options1', 'product', 'normal', 'high');
			add_meta_box('product_extra_options2','Dimensions', 'product_extra_options2', 'product', 'normal', 'high');
			add_meta_box('product_extra_options3','Variations', 'product_extra_options3', 'product', 'normal', 'high');
		}
	}
	
	function product_extra_options1($post_id) {
		$product_short = get_post_meta($post_id->ID, 'product_short', true);
?>
		<input type="hidden" name="cms_product_noncename" id="cms_product_noncename" value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
		<textarea name="product_short" style="width: 100%; height: 100px"><?php echo $product_short; ?></textarea>
<?php
	}
	
	
	function product_extra_options2($post_id) {
		$width  = htmlspecialchars(stripslashes(get_post_meta($post_id->ID, 'width', true)));
		$height = htmlspecialchars(stripslashes(get_post_meta($post_id->ID, 'height', true)));
		$length = htmlspecialchars(stripslashes(get_post_meta($post_id->ID, 'length', true)));
?>
		<input type="hidden" name="cms_product_noncename" id="cms_product_noncename" value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="33.3%">Width</td>
				<td width="33.3%" style="padding-left: 12px;">Height</td>
				<td style="padding-left: 12px;">Length</td>
			</tr>
			<tr>
				<td width="33.3%">
					<input type="text" style="width:100%" name="product_width" value="<?php echo $width; ?>" />
				</td>
				<td width="33.3%" style="padding-left: 12px;">
					<input type="text" style="width:100%" name="product_height" value="<?php echo $height; ?>" />
				</td>
				<td style="padding-left: 12px;">
					<input type="text" style="width:100%" name="product_length" value="<?php echo $length; ?>" />
				</td>
			</tr>
		</table>
<?php
	}
	
	function product_extra_options3($post_id) {
		error_reporting(0);
		
		$product_vars = get_post_meta($post_id->ID, 'product_vars', true);
?>
		<input type="hidden" name="cms_product_noncename" id="cms_product_noncename" value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
		<p><input type="button" class="button button-large add-variation button-primary" value="Add Variation"/></p>
		<div class="list-elements product-variations">
<?php
		if (is_array($product_vars['name'])) {
			$product_vars = $product_vars['name'];
			
			$count_vars = 0;
			
			foreach ($product_vars as $product_var) {
				$colors = $product_var['colors'];
?>
				<div class="rounded-div root-variations" style="padding-bottom: 10px;" data-num="<?php echo $count_vars; ?>">
					<div>
						<input class="var-name" name="variation[name][<?php echo $count_vars; ?>][title]" type="text" style="width: 400px" value="<?php echo $product_var['title']; ?>">
						<input type="button" class="button button-primary add-variation-color" value="Add Color">
						<input type="button" class="button remove-section-var" value="Remove Variation">
						<p><strong>Available Colors</strong></p>
						<div class="list-sub-elements" style="margin-top: 10px">
<?php
						if (is_array($colors)) {
							$count_colors = 0;
							
							foreach ($colors as $color) {
?>
								<div class="rounded-div root-colors" style="padding-bottom: 10px;background-color: #fff;background-image: none;" data-num="<?php echo $count_colors; ?>">
									<div>
										<input class="var-color" name="variation[name][<?php echo $count_vars; ?>][colors][<?php echo $count_colors; ?>][title]" type="text" style="width: 400px" value="<?php echo $color['title']; ?>">
										<input type="button" class="button button-primary add-variation-color-image" value="Add Image">
										<input type="button" class="button remove-section-color" value="Remove Color">
										<p><strong>Images</strong></p>
										<div class="list-sub-sub-elements">
<?php
											$images = $color['images'];
											
											if (is_array($images)) {
												foreach ($images as $image) {
													$image_src = wp_get_attachment_image_src($image, 'full');
?>
													<div class="section-image" style="width: 100px; margin-right: 10px; margin-bottom: 10px; float: left; text-align: center; background-color: #fff;">
														<img src="<?php echo $image_src[0]; ?>" width="100px">
														<input class="color_image" name="variation[name][<?php echo $count_vars; ?>][colors][<?php echo $count_colors; ?>][images][]" type="hidden" value="<?php echo $image; ?>">
														<a href="#" class="remove-section-image">Delete</a>
													</div>
<?php													
												}
											}
?>										
										</div>
										<div style="clear: both"></div>
									</div>
								</div>
<?php								
								$count_colors++;
							}
						}
?>						
						</div>
					</div>
				</div>
<?php		
				$count_vars++;				
			}
		}
?>		
		</div>
<?php
	}
	
	function product_save_postdata($post_id) {
		if ( !wp_verify_nonce( $_POST['cms_product_noncename'], plugin_basename(__FILE__) )) {
			return $post_id;
		}
			
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
			
		if ($_POST['post_type']=='product') {
			$product_short = $_POST['product_short'];
			
			if ($product_short != '') 
				update_post_meta($post_id, 'product_short', $product_short);
			else
				delete_post_meta($post_id, 'product_short');
			
			$width  = $_POST['product_width'];
			$height = $_POST['product_height'];
			$length = $_POST['product_length'];
			
			if ($width != '') 
				update_post_meta($post_id, 'width', $width);
			else
				delete_post_meta($post_id, 'width');
			
			if ($height != '') 
				update_post_meta($post_id, 'height', $height);
			else
				delete_post_meta($post_id, 'height');
			
			if ($length != '') 
				update_post_meta($post_id, 'length', $length);
			else
				delete_post_meta($post_id, 'length');
			
			$variations = $_POST['variation'];
			
			if (count($variations) > 0) 
				update_post_meta($post_id, 'product_vars', $variations);
			else
				delete_post_meta($post_id, 'product_vars');
		}
	}
?>