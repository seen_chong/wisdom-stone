// JavaScript Document
var geocoder;
var map;
var markersArray = [];


completed = true;

function rotate_slider() {
	if (jQuery('#nmSlider .nm-slide').length > 1) {
		if (completed) {
			this_len1   = parseInt(jQuery('#nmSlider .nm-slide').length); 
			this_index1 = parseInt(jQuery('#nmSlider .nm-slide').index(jQuery('#nmSlider .nm-slide.active')));
			next_index1 = this_index1 + 1;
			
			if (next_index1 >= this_len1) {
				next_index1 = 0; 
			}
			
			jQuery('#nmSlider .nm-slide.active').fadeOut(1000, function() {
				jQuery(this).removeClass('active');	
			});	
					
			jQuery('#nmSlider .nm-slide').eq(next_index1).fadeIn(1000, function() {
				jQuery('#nmSlider .nm-slide').eq(next_index1).addClass('active');	
			});
		}
	}
	
	setTimeout(function() {
		rotate_slider()
	}, 5000);
}

function rotate_slider_left() {
	if (jQuery('#nmSlider .nm-slide').length > 1) {
		if (completed) {
			this_len1   = parseInt(jQuery('#nmSlider .nm-slide').length); 
			this_index1 = parseInt(jQuery('#nmSlider .nm-slide').index(jQuery('#nmSlider .nm-slide.active')));
			next_index1 = this_index1 - 1;
			
			if (next_index1 > 0) {
				next_index1 = this_len1 - 1
			}
			
			jQuery('#nmSlider .nm-slide.active').fadeOut(1000, function() {
				jQuery(this).removeClass('active');	
			});	
						
			jQuery('#nmSlider .nm-slide').eq(next_index1).fadeIn(1000, function() {
				jQuery('#nmSlider .nm-slide').eq(next_index1).addClass('active');
				
				completed = true;	
			});
		}
	}
}

function rotate_slider_right() {
	if (jQuery('#nmSlider .nm-slide').length > 1) {
		if (completed) {
			this_len1   = parseInt(jQuery('#nmSlider .nm-slide').length); 
			this_index1 = parseInt(jQuery('#nmSlider .nm-slide').index(jQuery('#nmSlider .nm-slide.active')));
			next_index1 = this_index1 + 1;
			
			if (next_index1 >= this_len1) {
				next_index1 = 0; 
			}
			
			jQuery('#nmSlider .nm-slide.active').fadeOut(1000, function() {
				jQuery(this).removeClass('active');	
			});	
						
			jQuery('#nmSlider .nm-slide').eq(next_index1).fadeIn(1000, function() {
				jQuery('#nmSlider .nm-slide').eq(next_index1).addClass('active');	
				
				completed = true;	
			});
		}
	}
}

function fix_slider() {
	viewport_height = jQuery(window).height();
	
	jQuery('#nmSlider').css('height', viewport_height);
	jQuery('#nmSlider .col').css('height', parseInt(viewport_height) - 192);
	jQuery('#nmSlider .col .nm-slide').css('height', parseInt(viewport_height) - 192);
}

var sf, body;
var breakpoint = 1183;

jQuery(document).ready(function(e) {
	body = jQuery('body');
	
	jQuery('.owl-carousel .thumb').live('click', function(e) {
		e.preventDefault();
		
		img_src = jQuery(this).find('img').attr('src');
		
		jQuery('#product .fancybox').attr('href', img_src);
		jQuery('#product .fancybox img').attr('src', img_src);
	});
	
	fix_slider();
	
	jQuery(window).on('resize', function(){
		fix_slider();
	});
		
	sf = jQuery('#site-menu');
	
	if(body.width() >= breakpoint) {
		sf.superfish();	
	}
	else {
		jQuery('#site-menu a.submenu .plus').click(function(e) {
			e.preventDefault();
				
			jQuery(this).parent().next().animate({
				'height' : 'toggle'
			}, 1000);
		});
	}
	
	jQuery(window).resize(function() {
        if(body.width() >= breakpoint && !sf.hasClass('sf-js-enabled')) {
            sf.superfish('init');
			jQuery('#site-menu a.submenu .plus').unbind('click');
        }
		else if(body.width() < breakpoint) {
            sf.superfish('destroy');
			
			jQuery('#site-menu a.submenu .plus').click(function(e) {
				e.preventDefault();
				
				jQuery(this).parent().next().animate({
					'height' : 'toggle'
				}, 1000);
			});
        }
    });
	
	setTimeout(function() {
		rotate_slider()
	}, 5000);

	jQuery('.nm-dirNav .prev').click(function(e) {
		e.preventDefault();
		rotate_slider_left()
	});

	jQuery('.nm-dirNav .next').click(function(e) {
		e.preventDefault();
		rotate_slider_right()
	});
	
	jQuery('#scroll-down').click(function(e) {
		e.preventDefault();
		jQuery('html,body').animate({
		   scrollTop: jQuery("main").offset().top - 80
		});
	});

	jQuery('#icon-Menu').click(function(e) {
		e.preventDefault();

		if (jQuery('.sf-menu').css('display') == 'none') {
			jQuery('#icon-Menu').addClass('opened');
			jQuery(this).next().css('display', 'block')	
		}
		else {
			jQuery('#icon-Menu').removeClass('opened');	
			jQuery(this).next().css('display', 'none')	
		}
	});
	
	jQuery('#search-box').click(function(e) {
		e.preventDefault();
		
		var target = jQuery( e.target );

		if (target.is("input")) {
		
		}
		else {
			if (jQuery('#search-form input').hasClass('opened')) {
				jQuery('#search-form input').css('right', '-310px');
				jQuery('#search-form input').removeClass('opened');	
				setTimeout(function() {
					jQuery('#search-box a span').css('display', 'block');	
						
					jQuery('#search-form').css('z-index', '-1');
				}, 1000);
			}
			else {
				jQuery('#search-form').css('z-index', '200000');
				jQuery('#search-box a span').css('display', 'none');	
				jQuery('#search-form input').css('right', 0);	
				jQuery('#search-form input').addClass('opened');
			}
		}
	});
	
	jQuery('a.scroll-text').click(function(e) {
		e.preventDefault()
		
		jQuery('html,body').animate({
			scrollTop: jQuery(".main-text p:eq(0)").offset().top - parseFloat(jQuery('#header').height()) - 60 
		}, 300);
	});	 
	
	jQuery('a.scroll-products').click(function(e) {
		e.preventDefault()
		
		jQuery('html,body').animate({
			scrollTop: jQuery(".products-entries").offset().top - parseFloat(jQuery('#header').height()) - 60 
		}, 300);
	});	 
	
	jQuery('.select-box select').change(function(e) {
		e.preventDefault();
		
		this_text = jQuery(this).find('option:selected').text();
		this_val  = jQuery(this).val();
		
		jQuery(this).prev().html(this_text);
		
		if (this_val != '') {
			varations = JSON.parse(jQuery('#variations').val());

			if (jQuery(this).attr('id') == 'polish-values') {
				console.log(jQuery('#color-values').css('display'));
				if (jQuery('#color-values').css('display') == 'none') {
					polish_value = jQuery('#polish-values').val();
					product_id   = jQuery('#product_id').val();	
					
					jQuery.ajax({
						type 	 : "post",
						url 	  : ajaxurl,
						dataType : 'json',
						data 	 : {
							action  : 'load_images_simple', 
							polish  : polish_value, 
							product : product_id
						},
						success  : function(response) {
							jQuery("#product .owl-carousel").data('owlCarousel').destroy();
							jQuery("#product .owl-carousel").html('');
							
							images = response.msg;
							
							for (var i = 0; i < images.length; i++) {
								this_image = images[i][0];
									
								if (i == 0) {
									jQuery('#product .fancybox').attr('href', this_image);	
									jQuery('#product .fancybox img').attr('src', this_image);	
								}
								
								html = '<a class="thumb" href="' + this_image + '"> \
											<img class="img-responsive" src="' + this_image + '" width="133" height="134" alt=" "> \
										</a>';
										
								jQuery("#product .owl-carousel").append(html);
							}
							
							jQuery("#product .owl-carousel").owlCarousel({
								itemsCustom : [
									[0, 2],
									[480, 2],
									[600, 4],
									[980, 3],
									[1200, 3],
									[1400, 4],
									[1600, 4]
								],
								navigation: true,
								pagination:false,
								autoPlay: false
							});
						}
					})	
				}
				else {
					colors = varations[this_val]['colors'];
					
					jQuery('#color-values').html('<option value="">Available Crystal Colors</option>');
					jQuery('#color-values').prev().html('Available Crystal Colors');
					
					for (var k in colors) {
						each_color = varations[this_val]['colors'][k]['title'];
						
						jQuery('#color-values').append('<option value="' + k + '">' + each_color + '</option>');
					}
				}
			}
			else {
				polish_value = jQuery('#polish-values').val();
				color_value  = jQuery('#color-values').val();
				product_id   = jQuery('#product_id').val();
				
				jQuery.ajax({
					type 	 : "post",
					url 	  : ajaxurl,
					dataType : 'json',
					data 	 : {
						action  : 'load_images', 
						polish  : polish_value, 
						color   : color_value,
						product : product_id
					},
					success  : function(response) {
						jQuery("#product .owl-carousel").data('owlCarousel').destroy();
						jQuery("#product .owl-carousel").html('');
						
						images = response.msg;
						
						for (var i = 0; i < images.length; i++) {
							this_image = images[i][0];
								
							if (i == 0) {
								jQuery('#product .fancybox').attr('href', this_image);	
								jQuery('#product .fancybox img').attr('src', this_image);	
							}
							
							html = '<a class="thumb" href="' + this_image + '"> \
										<img class="img-responsive" src="' + this_image + '" width="133" height="134" alt=" "> \
									</a>';
									
							jQuery("#product .owl-carousel").append(html);
						}
						
						jQuery("#product .owl-carousel").owlCarousel({
							itemsCustom : [
								[0, 2],
								[480, 2],
								[600, 4],
								[980, 3],
								[1200, 3],
								[1400, 4],
								[1600, 4]
							],
							navigation: true,
							pagination:false,
							autoPlay: false
						});
					}
				})	
			}
		}
	});
	
	
	if (jQuery('#dealer-map').length > 0) {
		initialize();
		
		jQuery('.search-store').click(function(e) {
			var address = document.getElementById('address').value;
															
			geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					clearOverlays();
					var coords = results[0].geometry.location;
																	
					lat = coords.lat();
					lnt = coords.lng();
					km  = jQuery('#radius').val();
					
					jQuery.ajax({
						type 	 : "post",
						url 	  : ajaxurl,
						dataType : 'json',
						data 	 : {
							action : 'search_store', 
							lat	: lat, 
							lnt	: lnt,
							km     : km 
						},
						success  : function(response) {
							if (response.return == 'error') {
								alert(response.msg);	
							}
							else {
								var bounds = new google.maps.LatLngBounds();
								
								jQuery('.dealers-listing').css('display', 'block');
								 
								stores = response.stores;
								
								pins   = [];
								
								jQuery('#num-stores').html(stores.length);
								jQuery('.dealers-listing ul').html('');
								
								for (var i = 0; i < stores.length; i++) {
									name = stores[i].name;	
									addr = stores[i].addr;	
									city = stores[i].city;	
									zip  = stores[i].zip;	
									stat = stores[i].stat;	
									cn   = stores[i].cn;	
									phon = stores[i].phon;	
									fax  = stores[i].fax;	
									emai = stores[i].emai;	
									url  = stores[i].url;	
									logo = stores[i].logo;	
									lat  = stores[i].lat;	
									lnt  = stores[i].lnt;
									
									info = '<div class="single-dealer single-d-map clearfix">';
									info+= '	<div class="dealer-address">';
									info+= '		<h4 class="dealer-title">' + name + '</h4>';
									info+= '		<p><span>Address:</span>' + addr + ', ' + stat + ', ' + cn + ', ' + zip + '</p>';
									info+= '		<p><span>Phone:</span>' + phon + '</p>';
									
									if (url != '') {
									info+= '		<p><span>Website:</span> <a href="' + url + '" target="_blank">' + url + '</a></p>';
									}
									
									info+= '	</div>';
									info+= '</div>';
									
									pins.push({
										'title' : name,
										'lat'   : lat,
										'lnt'   : lnt,
										'info'  : info
									});
									
									html = '<li>';
									html+= '	<div class="single-dealer clearfix">';
									html+= '		<div class="dealer-address">';
									html+= '			<h4 class="dealer-title">' + name + '</h4>';
									html+= '			<p><span>Address:</span>' + addr + ', ' + stat + ', ' + cn + ', ' + zip + '</p>';
									html+= '			<p><span>Phone:</span>' + phon + '</p>';
									
									if (url != '') {
									html+= '			<p><span>Website:</span> <a href="' + url + '" target="_blank">' + url + '</a></p>';
									}
									
									html+= '		</div>';
									info+= '	</div>';
									info+= '</li>';
									
									jQuery('.dealers-listing ul').append(html);									
								}
								
								var infoWindow = new google.maps.InfoWindow(), marker, i;
								
								for (i = 0; i < pins.length; i++) {
									var position = new google.maps.LatLng(pins[i]['lat'], pins[i]['lnt']);
									
									bounds.extend(position);
									
									var image = new google.maps.MarkerImage('http://archmail.deveserver.com/wp-content/themes/mailboxes/img/pin.png',
										// This marker is 129 pixels wide by 42 pixels tall.
										new google.maps.Size(38, 68),
										// The origin for this image is 0,0.
										new google.maps.Point(0,0),
										// The anchor for this image is the base of the flagpole at 18,42.
										new google.maps.Point(18, 42)
									);
																		
									marker = new google.maps.Marker({
										position: position,
										map     : map,
										title   : pins[i]['title'],
										icon 	: image
									});
									
									google.maps.event.addListener(marker, 'click', (function(marker, i) {
										return function() {

											infoWindow.setContent(pins[i]['info']);
											infoWindow.open(map, marker);
										}
									})(marker, i));
							
									map.fitBounds(bounds);
								}
								
								var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
									this.setZoom(11);
									google.maps.event.removeListener(boundsListener);
								});
							}
						}
					}) 
				} 
				else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			})
		});		
	}
});


function initialize() {
	geocoder = new google.maps.Geocoder();
														
	map = new google.maps.Map(
		document.getElementById('map_canvas'), {
			center: new google.maps.LatLng(34.0522342, -118.2436849),
			zoom: 11,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		}
	);
}
													
function clearOverlays() {
	if (markersArray) {
		for (i in markersArray) {
			markersArray[i].setMap(null);
		}
	}
}