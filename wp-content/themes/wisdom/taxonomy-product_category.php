<?php
	get_header();
	
	$cat = get_query_var('product_category');
	$cat = get_term_by('slug', $cat, 'product_category');
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section">
		<div class="container wow fadeIn main-text" data-wow-delay="0.4s"> 
			<h2 class="main-title"><?php echo $cat->name; ?></h2>
			<a href="#" class="scroll-products">
				<img class="aligncenter" src="<?php echo get_bloginfo('template_url'); ?>/images/Arrow-down.png" width="48" height="48" alt="">
			</a>
			<div class="products-entries clearfix"> 
			<?php
				$args = array(
					'post_type' 	  => 'product',
					'posts_per_page' => -1,
					'tax_query'	  => array(
						array(
							'taxonomy' => 'product_category',
							'terms'	=> $cat->term_id
						)
					)
				);
				
				$products = new WP_Query($args);
				
				if ($products->have_posts()) {
					while ($products->have_posts()) {
						$products->the_post();
						
						$feat_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
			?>
						<!-- --> 
						<a class="prod-entry wow fadeIn" data-wow-delay="0.4s" href="<?php echo get_permalink(get_the_ID()) ;?>">
							<img class="img-responsive aligncenter" src="<?php echo $feat_image; ?>" width="296" height="296" alt=" ">
							<h3><?php the_title(); ?></h3>
						</a> 
						<!-- --> 
			<?php
					}
				}
				else {
					echo '<p>No products found.</p>';		
				}
				
				wp_reset_postdata();
			?>
			</div>
		</div>
	</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
	
	get_footer();
?>