<?php
	/* Template name: Contact */
	get_header();
	
	the_post();
		
	$contact_field1 = get_post_meta(get_the_ID(), 'contact_field1', true);
	$contact_field2 = get_post_meta(get_the_ID(), 'contact_field2', true);
	$contact_field3 = get_post_meta(get_the_ID(), 'contact_field3', true);
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section">
		<div class="container wow fadeIn main-text" data-wow-delay="0.4s">
			<h2 class="main-title uppercase">Contact us</h2>
			<a href="#" class="scroll-text">
				<img class="aligncenter" src="<?php echo get_bloginfo('template_url'); ?>/images/Arrow-down.png" width="48" height="48" alt="">
			</a>
			<div class="contact ptop60px clearfix">
				<div class="col info">
					<p>
						Please contact us at:
					</p>
					<p>
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-Location.png" width="42" height="42" alt="Location">
						<?php echo $contact_field1; ?>
					</p>
					<p>
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-Phone.png" width="42" height="42" alt="Phone">
						<?php echo $contact_field2; ?>
					</p>
					<p>
						<img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-Fax.png" width="42" height="42" alt="Fax">
						<?php echo $contact_field3; ?>
					</p>
				</div>
				<div class="col">
				<?php
					echo do_shortcode('[gravityform id=1 title=false description=false ajax=false tabindex=49]');
				?>
				</div>
				<script type="text/javascript">
					jQuery('form').addClass('default');
					jQuery('.gfield_label').remove();
					
					var left_field = '';
					
					jQuery('.gfield').each(function(index, element) {
						this_id = jQuery(this).attr('id');
						this_er = jQuery(this).hasClass('gfield_error');
						
						extra_cls = (this_er) ? ' class="gfield_error"' : '';
							
						jQuery('<div id="' + this_id + '"' + extra_cls + '></div>').appendTo('.gform_body');
						jQuery(this).find('.ginput_container').prependTo('form.default .gform_body div#' + this_id);
							
						if (this_er) {
							jQuery(this).find('.validation_message').appendTo('form.default .gform_body div#' + this_id);
						}
					});
					
					jQuery('.ginput_container select').css('opacity', 0);
					
					jQuery('<input class="default-btn send-message" type="button" value="send message">').appendTo('.gform_body');
					
					jQuery('#gform_fields_1').remove();
					
					jQuery('.send-message').click(function(e) {
						e.preventDefault();
						
						jQuery('#gform_submit_button_1').click();
					});
					
					jQuery(document).ready(function(e) {
				<?php
					if ($_POST['gform_submit'] != '') {
				?>
						jQuery('html,body').animate({
						   scrollTop: jQuery("#gform_1").offset().top - 130
						});				
				<?php		
					}
				?>	
					});
				</script>
			</div>
		</div>
	</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ --> 
</main>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *end MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
<?php
	
	get_footer();
?>