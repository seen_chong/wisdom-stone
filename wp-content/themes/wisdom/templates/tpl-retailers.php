<?php
	/* Template name: Retailers */
	get_header();
	
	the_post();
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section">
		<div class="container wow fadeIn main-text" data-wow-delay="0.4s"> 
			<h2 class="main-title uppercase">Browse Online Retailers</h2>
			<a href="#" class="scroll-text">
				<img class="aligncenter" src="<?php echo get_bloginfo('template_url'); ?>/images/Arrow-down.png" width="48" height="48" alt="">
			</a>
			<?php
				$args = array(
					'post_type' 	  => 'dealer',
					'posts_per_page' => -1,
					'orderby'	    => 'title',
					'order'		  => 'ASC'
				);
						
				$stores = new WP_Query($args);
			?>
			<h3 class="dealer-head">
				<span id="num-stores"><?php echo $stores->post_count; ?></span> Stores found
			</h3>
			<ul class="online-retailers">
			<?php
				while ($stores->have_posts()) {
					$stores->the_post();
							
			?>
					<li>
						<div class="single-dealer clearfix">
							<div class="dealer-address">
								<h4 class="dealer-title"><?php echo get_the_title(); ?></h4>
								<p>
									<span>website:</span>
									<?php echo get_the_content(); ?>
								</p>
							</div>
						</div>
					</li>
			<?php	
				}
						
				wp_reset_postdata();
			?>
			</ul>
		</div>
	</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
	
	get_footer();
?>