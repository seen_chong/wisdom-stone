<?php
	/* Template name: Stores */
	
	get_header();
	
	the_post();
?>
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=false"></script>
	<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section">
		<div class="container wow fadeIn main-text store-locator" data-wow-delay="0.4s"> 
			<div class="row">
			<div class="container">
				
				<div class="row">
					<form action="" method="post">
						<div class="form-group clearfix">
							<div class="col-3">
								<input type="text" class="form-control" id="address" name="address" placeholder="Address/ZipCode:">
							</div>
							<div class="col-3">
								<select name="radius" id="radius" class="form-control custom-select">
									<option value="5">5 miles</option>
									<option value="10">10 miles</option>
									<option value="50">50 miles</option>
									<option value="100">100 miles</option>
								</select>
							</div>
							<div class="col-8">
								<button class="btn btn-default btn-dark btn-lg search-store" type="button">SEARCH</button>
							</div>
						</div>
					</form>
					<p class="text-center">
						<a class="btn btn-default" href="<?php echo get_permalink(804); ?>">Browse Online Retailers</a>
					</p>					
				</div>
			</div>
		</div>  
        
		</div>
	</div>
    <div class="section">
    	<div  id="dealer-map">
            <div id="map_wrapper" >
                <div id="map_canvas" class="mapping"></div>
            </div>
        </div>	
    	<div class="container">
            <div class="row dealers-listing" style="display: none;">
                <div class="container">
                    <h3 class="dealer-head"><span id="num-stores"></span> Stores found</h3>
                    <ul></ul>
                </div>
            </div>
        </div>
    </div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php		
	get_footer();
?>