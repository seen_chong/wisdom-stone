<?php
	$feat_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

	if ($feat_image == '') {
		$feat_image = get_bloginfo('template_url') . '/pics/bg-body-empty.jpg';	
	}
	
	if (is_tax()) {
		$feat_image = get_bloginfo('template_url') . '/pics/bg-body-empty.jpg';	
	}
	
	if (is_singular()) {
		$feat_image = get_bloginfo('template_url') . '/pics/bg-body-empty.jpg';	
	}
	
	if ($_GET['s'] != '') {
		$feat_image = get_bloginfo('template_url') . '/pics/bg-body-empty.jpg';	
	}
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Wisdom Stone</title>
<!-- fonts -->
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!-- end fonts -->

<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/reset.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/default.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/animate.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/owl.carousel.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/media.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/custom.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/assets/fonts/font.css">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

<!-- css3-mediaqueries.js for IE less than 9 -->
<!--[if lt IE 9]>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
<script src='http://code.jquery.com/jquery-migrate-1.2.1.min.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/modernizr.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/hoverIntent.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/superfish.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/wow.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/owl.carousel.min.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/scripts.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.pack.js?v=2.1.5'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js'></script>
<script>
	// jQuery(document).on("scroll", function(){
	// 	if (jQuery(document).scrollTop() > 1){
	// 		jQuery("header").addClass("shrink");
	// 	}
	// 	else {
	// 		jQuery("header").removeClass("shrink");
	// 	}
	// }); 
</script>
<?php
	echo wp_head();

	if (get_the_ID() == '2') {
		$body_cls = ' class="home" ';
	}
        else {
		$body_cls = ' class="inner" ';
        }
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2097606-3', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body <?php body_class(); ?> >
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *HEADER
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<header role="banner" id="header" style="background-color:#fff;">
	<div class="container">
		<h1 class="logo">
			<a href="<?php echo get_bloginfo('url'); ?>" title="Wisdom Stone Homepage">
				<img class="img-responsive" src="<?php echo get_bloginfo('template_url'); ?>/images/logo-new.png" alt="Wisdom Stone logo">
			</a>
		</h1>
		<nav id="site-menu" class="clearfix">
			<a id="icon-Menu" href="#"><span></span></a>
			<ul class="sf-menu">
			<?php
				$defaults = array(
					'menu'            => '4',
					'container'       => '',
					'items_wrap'      => '%3$s'
				);
				
				wp_nav_menu($defaults);
			?>			
			</ul>
		</nav>
		<div id="search-box" class="clearfix">
			<form action="<?php echo get_bloginfo('url'); ?>" id="search-form">
				<input type="text" placeholder="Keywords:" value="<?php echo $_GET['s']; ?>" name="s" required id="s" aria-labelledby="search-label">
				<a class="search search-btn" href="">
					<!-- <span>SEARCH</span> -->
				</a>
			</form>
			<img class="shareIcon" src="<?php echo get_bloginfo('template_url'); ?>/pics/share-icon.png">
		</div>
	</div>
</header>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *end HEADER
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ --> 