<?php
	/* Template name: Home */
	get_header();
	
	the_post();
		
	$header_text1 = get_post_meta(get_the_ID(), 'header_text1', true);
	$header_link1 = get_post_meta(get_the_ID(), 'header_link1', true);
	$header_text2 = get_post_meta(get_the_ID(), 'header_text2', true);
	$header_link2 = get_post_meta(get_the_ID(), 'header_link2', true);

	$slider = get_post_meta(get_the_ID(), 'slider', true);
?>
<section id="nmSlider">
	<div class="container clearfix">
		<div class="col text">
			<?php the_content(); ?>
			<a class="transp default-btn" href="<?php echo $header_link1; ?>"><?php echo $header_text1; ?></a>
			<a class="transp default-btn" href="<?php echo $header_link2; ?>"><?php echo $header_text2; ?></a>
		</div>
		<div class="col image">
<?php
			if (is_array($slider)) {
				for ($i = 0; $i < count($slider['image']); $i++) {
					$id = $slider['image'][$i];
					$image = wp_get_attachment_image_src($id, 'full');
					
					$cls_image = ($i == 0) ? ' active' : '';
					$css_image = ($i == 0) ? '' : 'display: none; ';
?>
					<div class="nm-slide<?php echo $cls_image; ?>" style=" <?php echo $css_image; ?>background-image: url(<?php echo $image[0]; ?>)"> </div>
<?php
				}
			}
?>
		</div>
	</div>
</section>

<?php
	
	get_footer();
?>