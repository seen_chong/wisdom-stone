<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 

	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

     *FOOTER

	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->

	 <footer>
	 	<div class="footerWrapper">
	 		<div class="footerLogo">
	 			<img class="logo" src="<?php echo get_bloginfo('template_url'); ?>/pics/logo-new-fff.png">
	 		</div>
	 		<div class="footerLinks">
	 			<div class="footerLinkContact">
	 				<ul>
	 					<h6>Reach Out</h6>
	 					<li>Redondo Beach, CA</li>
	 					<li>800-464-7491</li>
	 					<li>310-374-5700</li>
	 					<li>310-374-5799</li>
	 					<li class="contactBtn">
	 						<a href="">
	 							<button>Contact Us</button>
	 						</a>
	 					</li>
	 					<li>
	 						<a href="">
	 							<button>Image Gallery</button>
	 						</a>
	 					</li>
	 					<li>
	 						<a href="">
	 							<button>Dealer Portal</button>
	 						</a>
	 					</li>
	 				</ul>
	 			</div>

	 			<div class="footerLinkNav">
	 				
	 				<ul>
	 					<h6>Explore</h6>
	 					<li><a href="">Home</a></li>
	 					<li><a href="">Where to buy</a></li>
	 					<li><a href="">Blog</a></li>
	 					<li><a href="">Products</a></li>
	 					<li><a href="">Online Retailers</a></li>
	 					<li><a href="">Contact Us</a></li>
	 				</ul>
	 			</div>

	 			<div class="footerLinkSocial">
					<ul>
						<h6>Follow Us</h6>
						<li>Facebook</li>
						<li>Twitter</li>
						<li>Pinterest</li>
						<li>Instagram</li>
						<li>YouTube</li>
					</ul>
	 			</div>
	 		</div>
	 	
	 	</div>
	 </footer>

	 <!-- OLD FOOTER -->

<!-- <footer role="contentinfo" id="footer">
	<div class="container clearfix wow fadeIn" data-wow-delay="0.4s">
		<div class="col clearfix">
			<ul class="social-media">
				<li class="title">
					Follow us on
				</li>
				<li>
					<a href="https://twitter.com/WisdomStoneHW?lang=en" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-Twitter.png" width="36" height="36" alt=" "></a>
				</li>
				<li>
					<a href="https://www.facebook.com/Wisdom-Stone-Hardware-785089454883513/?fref=ts" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-Facebook.png" width="36" height="36" alt=" "></a>
				</li>
				<li>
					<a href="https://www.youtube.com/channel/UCxkX2wW-3pi2rqlmXtwpbiA" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-youTube.png" width="36" height="36" alt=" "></a>
				</li>
				<li>
					<a href="https://www.pinterest.com/WisdomStoneHW/" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-Pinterest.png" width="36" height="36" alt=" "></a>
				</li>
				<li style="margin-right: 5px;">
					<a href="https://www.instagram.com/wisdomstonehw/" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-Insta.png" width="36" height="36" alt=" "></a>
				</li>
				<li style="margin-right: 2px;">
					<a href="https://plus.google.com/106937988638772125904" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/icon-Gp.png" width="36" height="36" alt=" "></a>
				</li>
			</ul>
		</div>
		<div class="col middle">
			<img class="img-responsive aligncenter" src="<?php echo get_bloginfo('template_url'); ?>/images/Widsom-Stone-logo-footer.png" width="186" height="70" alt=" ">
		</div>
		<div class="col" id="copyright"> &copy; 2015 Wisdom Stone, All Right Reserved.<br>
			Design by <a href="http://www.weberous.com" target="_blank" rel="nofollow">Weberous</a> </div>
	</div>
</footer> -->
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *end FOOTER
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ --> 
<script>
new WOW().init();

/* hamburger menu */
document.querySelector("#icon-Menu").addEventListener("click", function() {
	this.classList.toggle( "active" );
});
/* end hamburger menu */

jQuery(document).ready(function() {
	if (jQuery(".fancybox").length > 0) {
		jQuery(".fancybox").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none'
		});
	}
	
	var prodimgs = jQuery("#product .owl-carousel");
	
	if (prodimgs.length > 0) {
		prodimgs.owlCarousel({
			itemsCustom : [
				[0, 2],
				[480, 2],
				[600, 4],
				[980, 3],
				[1200, 3],
				[1400, 4],
				[1600, 4]
			],
			navigation: true,
			pagination:false,
			autoPlay: false
		});
	}
});
</script>
<style>

main[role="main"] {
    padding-top: 0 !important;
}

body.inner {
    padding-top: 168px;
}

#nm1Slider .image .nm-slide {
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -ms-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

#search-form {
    height: 36px;
}

#search-form input {
    height: 36px;
}
</style>
<?php
	echo wp_footer();
?>
</body>
</html>