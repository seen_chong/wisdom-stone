<?php
	function so_33280386_alter_assets() {
		wp_deregister_script( 'jquery' );
	}
	add_action('wp_enqueue_scripts', 'so_33280386_alter_assets', 10);

	add_filter('show_admin_bar', '__return_false');
	
	add_action('wp_head','pluginname_ajaxurl');
	
	function pluginname_ajaxurl() {
?>
		<script type="text/javascript">
			var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		</script>
<?php
	}

	add_theme_support('post-thumbnails');
	add_theme_support('menus');
	
	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
	
	function special_nav_class($classes, $item) {
		if(in_array('current-menu-item', $classes)) {
			$classes[] = 'current ';
		}
		return $classes;
	}
	
	include('framework/php/scripts.php');
	include('framework/php/page.php');
	include('framework/php/product.php');
	include('framework/php/store.php');

	function add_responsive_class($content){
		global $post;
   		$pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
   		$replacement = '<img$1class="$2 main-image img-responsive"$3>';
   		$content = preg_replace($pattern, $replacement, $content);
   		return $content;		
	}
	
	add_filter('the_content', 'add_responsive_class');
	
	add_action('wp_ajax_nopriv_send_contact', 'send_contact');
	add_action('wp_ajax_send_contact', 'send_contact');
	
	function send_contact() {
   		global $wpdb;
		
		ob_clean();
		
		$name  = $_POST['name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$text  = $_POST['message'];

		$to      = 'mario@weberous.com';
        $subject = 'Contact Form';
        $sender  = get_option('name');
                
        $message = '<strong>Name:</strong> ' . $name .'<br>';
        $message.= '<strong>E-mail:</strong> ' . $email .'<br>';
        $message.= '<strong>Phone:</strong> ' . $phone .'<br>';
        $message.= '<strong>Message:</strong> ' . $text .'<br>';
                
        $headers[] = 'MIME-Version: 1.0' . "\r\n";
        $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers[] = "X-Mailer: PHP \r\n";
        $headers[] = 'From:' . $name . '<' . $email .'>' . "\r\n";
                
        $mail = wp_mail($to, $subject, $message, $headers);
               
		$arr_status = array(
			'return' => 'success',
			'msg'	=> 'We sent you an email with the reset account link.'
		);	
		
		echo json_encode($arr_status);
		
		die();
	}
	
	add_action('wp_ajax_nopriv_load_images', 'load_images');
	add_action('wp_ajax_load_images', 'load_images');
	
	function load_images() {
   		global $wpdb;
		
		ob_clean();
		
		$polish  = $_POST['polish'];
		$color   = $_POST['color'];
        $product = $_POST['product'];
		
		$product_vars = get_post_meta($product, 'product_vars', true);
		$product_vars = $product_vars['name'];
		$product_imgs = $product_vars[$polish]['colors'][$color]['images'];
		
		$images  = array();
		
		foreach ($product_imgs as $product_img) {
			$images[] = wp_get_attachment_image_src($product_img, 'full');	
		}
		
		$arr_status = array(
			'return' => 'success',
			'msg'	=> $images
		);	
		
		echo json_encode($arr_status);
		
		die();
	}
	
	add_action('wp_ajax_nopriv_load_images_simple', 'load_images_simple');
	add_action('wp_ajax_load_images_simple', 'load_images_simple');
	
	function load_images_simple() {
   		global $wpdb;
		
		ob_clean();
		
		$polish  = $_POST['polish'];
		$color   = $_POST['color'];
        $product = $_POST['product'];
		
		$product_vars = get_post_meta($product, 'product_vars', true);
		$product_vars = $product_vars['name'];
		$product_imgs = $product_vars[$polish]['colors'][0]['images'];
		
		$images  = array();
		
		foreach ($product_imgs as $product_img) {
			$images[] = wp_get_attachment_image_src($product_img, 'full');	
		}
		
		$arr_status = array(
			'return' => 'success',
			'msg'	=> $images
		);	
		
		echo json_encode($arr_status);
		
		die();
	}
	
	add_action('wp_ajax_nopriv_search_store', 'search_store');
	add_action('wp_ajax_search_store', 'search_store');
	
	function search_store() {
   		global $wpdb;
		
		ob_clean();
		$lat = $_POST['lat'];
		$lnt = $_POST['lnt'];
		$km  = $_POST['km'];
		$lon1 = $lnt - $km/abs(cos(deg2rad($_GET['lat']))*69);
		$lon2 = $lnt + $km/abs(cos(deg2rad($_GET['lat']))*69);
		$lat1 = $lat - ($km/69);
		$lat2 = $lat + ($km/69);
		
		$km   = round($km * 1.609344);
		
		$sql = "SELECT id, 3956 * 2 * ASIN(SQRT(POWER(SIN((" . $lat . " - lat_store.meta_value) * pi()/180 / 2), 2) + COS(" . $lat . " * pi()/180) * COS(lat_store.meta_value * pi()/180) * POWER(SIN((" . $lnt . " - lnt_store.meta_value) * pi()/180 / 2), 2) )) as distance 
				from ".$wpdb->prefix."posts
				
				inner join ".$wpdb->prefix."postmeta as lat_store on (lat_store.post_id = ".$wpdb->prefix."posts.ID)
				inner join ".$wpdb->prefix."postmeta as lnt_store on (lnt_store.post_id = ".$wpdb->prefix."posts.ID)
					
				WHERE lnt_store.meta_key = 'store_lnt' and (lnt_store.meta_value between " . $lon1 . " and " . $lon2 . ") and lat_store.meta_key = 'store_lat' and (lat_store.meta_value between " . $lat1 . " and " . $lat2 . ") 
									
				having distance <= " . $km;
		
		$stores = $wpdb->get_results($sql);
		
		if (count($stores) > 0) {
			foreach ($stores as $store) {
				$store_info    = get_post($store->id);
				$store_name    = $store_info->post_title;	
				$store_address = get_post_meta($store->id, 'store_address', true);
				$store_city  	= get_post_meta($store->id, 'store_city', true);
				$store_zip 	 = get_post_meta($store->id, 'store_zip', true);
				$store_state   = get_post_meta($store->id, 'store_state', true);
				$store_cn      = get_post_meta($store->id, 'store_cn', true);
				$store_phone   = get_post_meta($store->id, 'store_phone', true);
				$store_fax     = get_post_meta($store->id, 'store_fax', true);
				$store_email   = get_post_meta($store->id, 'store_email', true);
				$store_url 	 = get_post_meta($store->id, 'store_url', true);
				$store_logo    = get_post_meta($store->id, 'store_logo', true);
				$store_lat     = get_post_meta($store->id, 'store_lat', true);
				$store_lnt     = get_post_meta($store->id, 'store_lnt', true);
				
				if ($store_cn == '') {
					$store_cn = 'USA';	
				}
				
				$single_store[] = array(
					'name' => $store_name,
					'addr' => $store_address,
					'city' => $store_city,
					'zip'  => $store_zip,
					'stat' => $store_state,
					'cn'   => $store_cn,
					'phon' => $store_phone,
					'fax'  => $store_fax,
					'emai' => $store_email,
					'url'  => $store_url,
					'logo' => $store_logo,
					'lat'  => $store_lat,
					'lnt'  => $store_lnt
				);
				$arr_status = array(
					'return' => 'success',
					'stores' => $single_store,
				);
			}
		}
		else {
			$arr_status = array(
				'return' => 'error',
				'msg'    => 'No stores found',
			);
		}
		
		echo json_encode($arr_status);
		
		die();
	}
	
	add_action('init', 'create_posttype');
	
	function create_posttype() {
		register_post_type('dealer',
			array(
				'labels'   => array(
					'name' => __( 'Dealers' ),
					'singular_name' => __( 'Dealer' )
				),
				'public'      => true,
				'has_archive' => true,
				'rewrite'     => array('slug' => 'dealer')
			)
		);
	
		register_post_type('store',
			array(
				'labels'   => array(
					'name' => __( 'Stores' ),
					'singular_name' => __( 'Store' )
				),
				'public'      => true,
				'has_archive' => true,
				'rewrite'     => array('slug' => 'store')
			)
		);
  		
		register_post_type('product',
    		array(
      			'labels' => array(
        			'name' 		  => __('Products'),
        			'singular_name' => __('Product')
      			),
      			'public' => true,
      			'has_archive' => true,
      			'rewrite' => array('slug' => 'product'),
				'supports' => array( 'title', 'editor', 'thumbnail' )
    		)
  		);
		
		$labels = array(
			'name'              => _x('Product Categories', 'taxonomy general name'),
			'singular_name'     => _x('Product Category', 'taxonomy singular name'),
			'search_items'      => __('Search Product Categories'),
			'all_items'         => __('All Product Categories'),
			'parent_item'       => __('Parent Product Category'),
			'parent_item_colon' => __('Parent Product Category:'),
			'edit_item'         => __('Edit Product Category'),
			'update_item'       => __('Update Product Category'),
			'add_new_item'      => __('Add New Product Category'),
			'new_item_name'     => __('New Product Category Name'),
			'menu_name'         => __('Product Categories'),
		);
	
		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array('slug' => 'product_category'),
		);
	
		register_taxonomy('product_category', array('product'), $args);
	}

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}
	
// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}
	// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-left' => __('Header Left'), // Main Navigation Left
        'header-right' => __('Header Right'), // Main Navigation Right
        'mobile-menu' => __('Mobile Menu'), // Mobile Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Shop Filter',
    'before_title' => '<h4>',
    'after_title' => '</h4>',
  )
);

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}
?>