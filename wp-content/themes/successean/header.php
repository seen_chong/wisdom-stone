<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package successean
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'successean' ); ?></a>

<header role="banner" id="header" class="wow fadeIn" data-wow-delay="0.3s">
	<div class="container">
		<h1 class="logo">
			<a href="<?php echo get_bloginfo('url'); ?>" title="Wisdom Stone Homepage">
				<img class="img-responsive" src="<?php echo get_bloginfo('template_url'); ?>/images/logo-new.png" alt="Wisdom Stone logo">
			</a>
		</h1>
		<nav id="site-menu" class="clearfix">
			<a id="icon-Menu" href="#"><span></span></a>
			<ul class="sf-menu">
			<?php
				$defaults = array(
					'menu'            => '4',
					'container'       => '',
					'items_wrap'      => '%3$s'
				);
				
				wp_nav_menu($defaults);
			?>			
			</ul>
		</nav>
		<div id="search-box" class="clearfix">
			<form action="<?php echo get_bloginfo('url'); ?>" id="search-form">
				<input type="text" placeholder="Keywords:" value="<?php echo $_GET['s']; ?>" name="s" required id="s" aria-labelledby="search-label">
				<a class="search search-btn" href="">
					<!-- <span>SEARCH</span> -->
				</a>
			</form>
		</div>
	</div>
</header>


