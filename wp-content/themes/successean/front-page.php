<?php

	get_header();

?>
<div class="homeHero" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/home-hero.jpg');">
	<div class="homeHeroContent">
		<h1>Dazzling</h1>
		<h4>adornments for your home.</h4>
		<a href="">
			<button>Browse All</button>
		</a>
	</div>
</div>

<div class="homeSectionOne" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/gold-rings.png');">
	<div class="homeSectionOneLeft">

	</div>
	<div class="homeSectionOneRight">
		<div class="homeSectionOneRightText">
			<img class="dash" src="<?php echo get_bloginfo('template_url'); ?>/pics/gold-dash.png">

			<h4>One of a kind variety,<br> for every type of cabinet</h4>
			<p>Wisdom Stone is a unique new brand of decorative cabinet hardware with over 300 different styles in a variety of finishes and colored crystals. You won’t find hardware quite like it.</p>
			<a href="">Learn More</a>
		</div>
	</div>
</div>

<div class="homeSectionTwo" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/purple-rings.png');">
	<div class="homeSectionTwoLeft" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/nice-knobs.jpg');">
		<div class="homeSectionTwoLeftText">
			<h3>Lasting Quality</h3>
			<p>Wisdom Stone is a unique new brand of decorative cabinet hardware with over 300 different styles in a variety of finishes and colored crystals. You won’t find hardware quite like it.</p>
			<a href="">View Products</a>
		</div>
	</div>
	<div class="homeSectionTwoRight">

	</div>
</div>

<div class="newsletterSection">
	<div class="newsletterSectionWrapper">
		<h5>Get our latest products and expert design tips right in your inbox</h5>
		<form class="newsletterForm">
		  <input type="email" name="firstname" placeholder="Your Email Address">
		  <input type="submit" value="Sign Up">
		</form>
	</div>
</div>

<div class="midHero" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/mid-hero.jpg');">
	<div class="midHeroBorder">
		<div class="midHeroContent">
			<h1>Decorative</h1>
			<h4>Unique Sleek Durable</h4>
			<a href="">
				<button>Learn More</button>
			</a>
		</div>
	</div>
</div>

<div class="sectionShowcase">
	<div class="sectionShowcaseContainer">
		<div class="sectionShowcaseHeader">
			<h2>Cabinetry adornments for <br>your every need</h2>
			<p>We offer a wide variety of decorative adornments for every style of home, including: wall mount mailboxes, locking mailboxes, multi-home mailboxes, and many more. Browse our extensive inventory of secure mailboxes to find the design that works for you and your home. We also offer an array of security features to keep your mail safe from the threat of prying eyes or identity theft.  </p>
		</div>
		<div class="sectionShowcaseContent">
			<div class="topSection" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/knob-showcase.jpg');">
				<div class="topSectionLeft">
					<h4>Knobs</h4>
					<h6>14 styles to choose from</h6>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<a href="">Learn More</a>
				</div>
				<div class="topSectionRight">
					<div class="topSectionRightBorder">

					</div>
				</div>
			</div>
			<div class="bottomSection" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/pull-showcase.jpg');">
				<div class="bottomSectionLeft">
				</div>
				<div class="bottomSectionRight">
					<h4>Pulls</h4>
					<h6>14 styles to choose from</h6>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<a href="">Learn More</a>
				</div>

			</div>
		</div>
		<div class="showcaseRetailers">
			<img class="dash" src="<?php echo get_bloginfo('template_url'); ?>/pics/gold-dash.png">
			<h4>Available at these Retailers</h4>
			<div class="retailerIcons">
				<img src="<?php echo get_bloginfo('template_url'); ?>/pics/retailer-lowes.jpg">
				<img src="<?php echo get_bloginfo('template_url'); ?>/pics/retailer-homedepot.jpg">
				<img src="<?php echo get_bloginfo('template_url'); ?>/pics/retailer-amazon.jpg">
				<img src="<?php echo get_bloginfo('template_url'); ?>/pics/retailer-lowes.jpg">
				<img src="<?php echo get_bloginfo('template_url'); ?>/pics/retailer-homedepot.jpg">
				<img src="<?php echo get_bloginfo('template_url'); ?>/pics/retailer-amazon.jpg">
			</div>
		</div>
	</div>
</div>


<?php
	
	get_footer();
?>