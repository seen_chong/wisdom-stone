<?php
# Database Configuration
define( 'DB_NAME', 'wisdomstone_dev' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'root' );
define( 'DB_HOST', '127.0.0.1' );
define( 'DB_HOST_SLAVE', '127.0.0.1' );
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');
$table_prefix = 'wp_';

define('WP_HOME','http://wisdom-stone.dev');
define('WP_SITEURL','http://wisdom-stone.dev');

# Security Salts, Keys, Etc
define('AUTH_KEY',         '%R,}QA|9!+~ |*c/+nTh_AC_-b A-{(-*8uiY^0q3(l]oEhi=V8.>52YU@aqW:Z*');
define('SECURE_AUTH_KEY',  'E )*x+V$cRTMR.c-%,6|cVe>#N`V#&zMa2L-c+7b9rUVSO A&V&AdTDBI3Xf=PnW');
define('LOGGED_IN_KEY',    '~Ov2+Qx->/B^^k]G+mYM$qp)8#PmSRSw./`Hcq34oP-:(So~SCm(qD|oh_w~McmL');
define('NONCE_KEY',        '{>|)[0I=Z3ufe*DbH]Djq=Gz+)zvS?W|a)gpu~;lP:u^e2Ilk;%U_,5qWgpWTCqq');
define('AUTH_SALT',        'ZS^#{/YfF.N?cI:|$lb|Kk`h0+G1s@>ikvYwlnC_WI1Q>/}*w59HYTc~1/Q#tp}m');
define('SECURE_AUTH_SALT', '[%GEJ#T9$!r_0%&dTb~x:_QuO7>x2tnd9~3i%Y%-e`c]8?I|O#Yhr_yrEPV?P*IS');
define('LOGGED_IN_SALT',   '@}7cG~hNd+z ,OJbftKQ[0/diq#vTWX_l=ukQv=!^QWHf*g.nbH- f/$fiZsyS%*');
define('NONCE_SALT',       'JY,b:%;4m[gt%EH%Z8M)C_5O02s>q;:vS.zIovT+|5vOqLB-cr<$7(3c.i/`z>D-');


# Localized Language Stuff

define( 'WP_CACHE', TRUE );

define( 'WP_AUTO_UPDATE_CORE', false );

define( 'PWP_NAME', 'wisdomstone' );

define( 'FS_METHOD', 'direct' );

define( 'FS_CHMOD_DIR', 0775 );

define( 'FS_CHMOD_FILE', 0664 );

define( 'PWP_ROOT_DIR', '/nas/wp' );

define( 'WPE_APIKEY', 'a0728271d0a6c3fec8db25e885c5931d620a43e7' );

define( 'WPE_FOOTER_HTML', "" );

define( 'WPE_CLUSTER_ID', '100148' );

define( 'WPE_CLUSTER_TYPE', 'pod' );

define( 'WPE_ISP', true );

define( 'WPE_BPOD', false );

define( 'WPE_RO_FILESYSTEM', false );

define( 'WPE_LARGEFS_BUCKET', 'largefs.wpengine' );

define( 'WPE_SFTP_PORT', 2222 );

define( 'WPE_LBMASTER_IP', '' );

define( 'WPE_CDN_DISABLE_ALLOWED', true );

define( 'DISALLOW_FILE_MODS', FALSE );

define( 'DISALLOW_FILE_EDIT', FALSE );

define( 'DISABLE_WP_CRON', false );

define( 'WPE_FORCE_SSL_LOGIN', false );

define( 'FORCE_SSL_LOGIN', false );

/*SSLSTART*/ if ( isset($_SERVER['HTTP_X_WPE_SSL']) && $_SERVER['HTTP_X_WPE_SSL'] ) $_SERVER['HTTPS'] = 'on'; /*SSLEND*/

define( 'WPE_EXTERNAL_URL', false );

define( 'WP_POST_REVISIONS', FALSE );

define( 'WPE_WHITELABEL', 'wpengine' );

define( 'WP_TURN_OFF_ADMIN_BAR', false );

define( 'WPE_BETA_TESTER', false );

umask(0002);

$wpe_cdn_uris=array ( );

$wpe_no_cdn_uris=array ( );

$wpe_content_regexs=array ( );

$wpe_all_domains=array ( 0 => 'wisdom-stone.com', 1 => 'www.wisdom-stone.com', 2 => 'wisdomstone.wpengine.com', );

$wpe_varnish_servers=array ( 0 => 'pod-100148', );

$wpe_special_ips=array ( 0 => '104.196.172.71', );

$wpe_ec_servers=array ( );

$wpe_largefs=array ( );

$wpe_netdna_domains=array ( );

$wpe_netdna_domains_secure=array ( );

$wpe_netdna_push_domains=array ( );

$wpe_domain_mappings=array ( );

$memcached_servers=array ( );


# WP Engine ID


# WP Engine Settings






define('WP_DEBUG', false);

# That's It. Pencils down
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');

$_wpe_preamble_path = null; if(false){}
